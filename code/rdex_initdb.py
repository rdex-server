# ======================================================================
# rdex -- reaction-diffusion explorer
# Copyright (C) 2008,2009,2019 Claude Heiland-Allen <claude@mathr.co.uk>
# License http://www.fsf.org/licensing/licenses/agpl-3.0.html
# ----------------------------------------------------------------------
# rdex server
# ======================================================================

# ======================================================================
# imports
import os
import sys
import string
import MySQLdb

# ======================================================================
# configuration
PROJECTDIR = os.path.dirname(__file__)
sys.path.append(PROJECTDIR)

from rdex_config import *

# ======================================================================
# database connector
def dbconnect():
  return MySQLdb.connect(
    host   = database_host,
    user   = database_user,
    passwd = database_pass,
    db     = database_db,
    unix_socket = database_sock)

def create():
  query = [ ]
  query.append('''
    CREATE TABLE rdex (
        id INTEGER NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
        ru DOUBLE NOT NULL,
        rv DOUBLE NOT NULL,
        f DOUBLE NOT NULL,
        k DOUBLE NOT NULL,
        behaviour VARCHAR(255),
        coarseness_u DOUBLE,
        contrast_u DOUBLE,
        directionality_u DOUBLE,
        coarseness_v DOUBLE,
        contrast_v DOUBLE,
        directionality_v DOUBLE
    ''')
  for r in range(0,8):
    for g in range(0,8):
      for b in range(0,8):
        query.append(', hist_%s_%s_%s DOUBLE' % (r, g, b))
  for s in range(0,32):
    query.append(', seg_%s DOUBLE' % s)
  query.append(')')
  query = string.join(query, '')
  conn = dbconnect()
  cursor = conn.cursor()
  cursor.execute(query)
  cursor.close()
  conn.close()
