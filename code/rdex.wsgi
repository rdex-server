# ======================================================================
# rdex -- reaction-diffusion explorer
# Copyright (C) 2008,2009,2019 Claude Heiland-Allen <claude@mathr.co.uk>
# License http://www.fsf.org/licensing/licenses/agpl-3.0.html
# ----------------------------------------------------------------------
# rdex server
# ======================================================================

# ======================================================================
# imports
from webob import Request, Response, exc
import re
import math
import string
import random
import os
import sys
import MySQLdb
from PIL import Image

# ======================================================================
# configuration
PROJECTDIR = os.path.dirname(__file__)
sys.path.append(PROJECTDIR)

from rdex_config import *

colour_buckets = 8
segment_buckets = 32
emptyOutput = [ '<div id="empty">(nothing found)</div>' ]

# ======================================================================
# style
focus_rows = 2
focus_cols = 2
gallery_rows = 8
gallery_cols = 8
thumbnailsize = (64, 64)

def toggleclass(name, mode):
  if name == mode:
    return "active"
  else:
    return "inactive"

def theme_page(mode, body, id = None, count = None, coords = None):
  title = "rdex :: %s :: %s" % ( mountpointname, mode )
  idstr = ""
  if id != None:
    idstr = "/%s" % id
    title = "%s :: %s" % ( title, id)
  latest = ""
  if count != None:
    latest = latesttable(mode, count)
  coordsstr = "&nbsp;"
  if coords != None:
    coordsstr = "<dl><dt>ru</dt><dd>%s</dd><dt>rv</dt><dd>%s</dd><dt>f</dt><dd>%s</dd><dt>k</dt><dd>%s</dd></dl>" % ( coords['ru'],  coords['rv'],  coords['f'],  coords['k'] )
  return '''
<html>
  <head>
    <title>%s</title>
    <link rel="shortcut icon" href="%s/rdex.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="%s/rdex.css" />
  </head>
  <body>
   <div id="page">
    <h1 id="logo"><a href="/" title="rdex"><img src="%s/rdex.png" alt="rdex" /></a></h1>
    <h2 id="logo2"><a href="%s/" title="%s"><img src="%s/%s" alt="%s" /></a></h2>
    <div id="prelatest">&nbsp;</div>
    <div id="latest">%s</div>
    <div id="navigation">
      <div id="premetrics">&nbsp;</div>
      <div id="metrics">
        <div class="toggle %s"><a href="%s/nearby%s"  title="nearby"  >nearby</a></div>
        <div class="toggle %s"><a href="%s/colour%s"  title="colour"  >colour</a></div>
        <div class="toggle %s"><a href="%s/texture%s" title="texture" >texture</a></div>
        <div class="toggle %s"><a href="%s/segment%s" title="segment" >segment</a></div>
      </div>
    </div>
    <div id="content">
      %s
    </div>
    <div id="footer">
      <div id="coords">%s</div>
    </div>
   </div>
  </body>
</html>
''' % ( title, style_url, style_url,
        style_url,
        mountpoint, mountpointname, style_url, mountpointicon, mountpointname,
        latest,
        toggleclass("nearby",  mode), mountpoint, idstr,
        toggleclass("colour",  mode), mountpoint, idstr,
        toggleclass("texture", mode), mountpoint, idstr,
        toggleclass("segment", mode), mountpoint, idstr,
        body,
        coordsstr )

# ======================================================================
# routing regex
var_regex = re.compile(r'''
  \{            # The exact character "{"
  (\w+)         # The variable name (restricted to a-z, 0-9, _)
  (?::([^}]+))? # The optional :regex part
  \}            # The exact character "}"
''', re.VERBOSE)

# ======================================================================
# routing templates
def template_to_regex(template):
  regex = ''
  last_pos = 0
  for match in var_regex.finditer(template):
    regex += re.escape(template[last_pos:match.start()])
    var_name = match.group(1)
    expr = match.group(2) or '[^/]+'
    expr = '(?P<%s>%s)' % (var_name, expr)
    regex += expr
    last_pos = match.end()
  regex += re.escape(template[last_pos:])
  regex = '^%s$' % regex
  return regex

# ======================================================================
# router class
class Router(object):
  def __init__(self):
    self.routes = []
  def add_route(self, template, controller, **vars):
    self.routes.append((re.compile(template_to_regex(template)), controller, vars))
  def __call__(self, environ, start_response):
    req = Request(environ)
    for regex, controller, vars in self.routes:
      match = regex.match(req.path_info)
      if match:
        req.urlvars = match.groupdict()
        req.urlvars.update(vars)
        return controller(environ, start_response)
    return exc.HTTPNotFound()(environ, start_response)

# ======================================================================
# redirect class
class Redirect(object):
  def __init__(self, url):
    self.url = url
  def __call__(self, environ, start_response):
    return exc.HTTPFound(location = self.url)(environ, start_response)

# ======================================================================
# redirect latest class
class RedirectLatest(object):
  def __init__(self, url):
    self.url = url
  def __call__(self, environ, start_response):
    conn = dbconnect ()
    count = getcount(conn)
    conn.close()
    if count == None: # or count < 1:
      return exc.HTTPNotFound()(environ, start_response)
    else:
      return exc.HTTPFound(location = ("%s/%s" % ( self.url, count)))(environ, start_response)


# ======================================================================
# redirect random class
class RedirectRandom(object):
  def __init__(self, url):
    self.url = url
  def __call__(self, environ, start_response):
    conn = dbconnect ()
    cursor = conn.cursor ()
    cursor.execute ("SELECT COUNT(id) FROM rdex")
    results = cursor.fetchall ()
    if cursor.rowcount < 1:
      cursor.close ()
      conn.close ()
      return exc.HTTPNotFound()(environ, start_response)
    else:
      count = 1
      for row in results:
        count = row[0]
      cursor.close ()
      conn.close ()
      id = random.randrange(count) + 1
      return exc.HTTPFound(location = ("%s/%s" % ( self.url, id)))(environ, start_response)

# ======================================================================
# controller wrapper
def rest_controller(cls, ctype = None):
  def replacement(environ, start_response):
    req = Request(environ)
    try:
      instance = cls(req, **req.urlvars)
      action = req.urlvars.get('action')
      if action:
        action += '_' + req.method.lower()
      else:
        action = req.method.lower()
      try:
        method = getattr(instance, action)
      except AttributeError:
        raise exc.HTTPNotFound("No action %s" % action)
      resp = method()
      if isinstance(resp, basestring):
        resp = Response(body=resp)
        if ctype != None:
          resp.content_type = ctype
    except exc.HTTPException, e:
      resp = e
    return resp(environ, start_response)
  return replacement

# ======================================================================
# database connector
def dbconnect():
  return MySQLdb.connect(
    host   = database_host,
    user   = database_user,
    passwd = database_pass,
    db     = database_db)

# ======================================================================
# database count helper
def getcount(conn):
  count = None
  cursor = conn.cursor ()
  cursor.execute ("SELECT COUNT(id) FROM rdex")
  results = cursor.fetchall ()
  if cursor.rowcount > 0:
    for row in results:
      count = row[0]
  cursor.close ()
  return count

# ======================================================================
# database coords helper
def getcoords(conn, id):
  coords = None
  cursor = conn.cursor ()
  cursor.execute ( "SELECT ru,rv,f,k FROM rdex WHERE id = '%s'" % conn.escape_string('%s'%id) )
  results = cursor.fetchall ()
  if cursor.rowcount > 0:
    coords = { }
    for row in results:
      coords['ru'] = row[0]
      coords['rv'] = row[1]
      coords['f']  = row[2]
      coords['k']  = row[3]
  cursor.close ()
  return coords

# ======================================================================
# database row fetcher helper
def fetchids(cursor):
  ids = [ ]
  for y in range(0,gallery_rows):
    for x in range(0,gallery_cols):
      row = cursor.fetchone ()
      if row != None:
        ids.append(row['id'])
      else:
        ids.append(None)
  return ids

# ======================================================================
# focus table creator
def focustable(id):
  output = [ ]
  output.append('<table id="focused">')
  for y in range(0,focus_rows):
    output.append('<tr>')
    for x in range(0,focus_cols):
      output.append('<td><img src="%s/%s.png" alt="%s" /></td>' % ( image_url, id, id ) )
    output.append('</tr>')
  output.append('</table>')
  return string.join(output, '')

# ======================================================================
# latest table creator
def latesttable(mode, id):
  output = [ ]
  output.append('<table>')
  output.append('<tr>')
  for x in range(0,12):
    if (id > 0):
      output.append(thumbnailcell(mode, id))
    else:
      output.append("<td>&nbsp;</td>")
    id = id - 1
  output.append('</tr>')
  output.append('</table>')
  return string.join(output, '')

# ======================================================================
# thumbnail creator
def thumbnailcell(mode,id):
  return '<td><a href="%s/%s/%s" title="%s"><img src="%s/%s.jpg" alt="%s" /></a></td>' % ( mountpoint, mode, id, id, image_url, id, id )

# ======================================================================
# thumbnail table creator
def thumbnailtable(mode, ids):
  output = [ ]
  output.append('<table id="related">')
  i = 0
  for y in range(0,gallery_rows):
    output.append('<tr>')
    for x in range(0,gallery_cols):
      if (ids[i]) != None:
        output.append(thumbnailcell(mode, ids[i]))
      else:
        output.append('<td>&nbsp;</td>')
      i = i + 1
    output.append('</tr>')
  output.append('</table>')
  return string.join(output, '')

# ======================================================================
# gallery texture class
class GalleryTexture(object):
  def __init__(self, req, id):
    self.request = req
    self.id = id
  def get(self):
    conn = dbconnect ()
    cursor = conn.cursor (MySQLdb.cursors.DictCursor)
    cursor.execute ("SELECT coarseness_u,contrast_u,directionality_u,coarseness_v,contrast_v,directionality_v FROM rdex WHERE id = '%s'" % conn.escape_string('%s'%self.id))
    me = cursor.fetchone()
    if me != None:
      cursor.close ()
      cursor = conn.cursor (MySQLdb.cursors.DictCursor)
      cursor.execute ('''
        SELECT id FROM rdex ORDER BY (
        (ABS(coarseness_u - '%s') / 0.11) + (ABS(contrast_u - '%s') / 0.16) + (ABS(directionality_u - '%s') / 0.51) +
        (ABS(coarseness_v - '%s') / 0.11) + (ABS(contrast_v - '%s') / 0.16) + (ABS(directionality_v - '%s') / 0.51) ) ASC
        ''' % (
        conn.escape_string('%s'%me['coarseness_u']), conn.escape_string('%s'%me['contrast_u']), conn.escape_string('%s'%me['directionality_u']),
        conn.escape_string('%s'%me['coarseness_v']), conn.escape_string('%s'%me['contrast_v']), conn.escape_string('%s'%me['directionality_v']) ) )
      output = [ ]
      row = cursor.fetchone ()
      if row != None:
        id = row['id']
        output.append(focustable(id))
        output.append(thumbnailtable('texture', fetchids(cursor)))
      else:
        output = emptyOutput
    else:
      output = emptyOutput
    count = getcount(conn)
    coords = getcoords(conn, self.id)
    conn.close ()
    return theme_page('texture', string.join(output, ''), self.id, count, coords)

# ======================================================================
# gallery colour class
class GalleryColour(object):
  def __init__(self, req, id):
    self.request = req
    self.id = id
  def get(self):
    conn = dbconnect ()
    cursor = conn.cursor (MySQLdb.cursors.DictCursor)
    query = []
    query.append("SELECT id")
    for r in range(0,colour_buckets):
      for g in range(0,colour_buckets):
        for b in range(0,colour_buckets):
          query.append(",hist_%s_%s_%s" % (r,g,b))
    query.append(" FROM rdex WHERE id = '%s'" % conn.escape_string('%s'%self.id))
    query = string.join(query, '')
    cursor.execute(query)
    me = cursor.fetchone()
    if me != None:
      cursor.close ()
      cursor = conn.cursor (MySQLdb.cursors.DictCursor)
      query = []
      query.append("SELECT id FROM rdex ORDER BY (")
      for r in range(0,colour_buckets):
        for g in range(0,colour_buckets):
          for b in range(0,colour_buckets):
            query.append("ABS(hist_%s_%s_%s - '%s') + " % (r,g,b,conn.escape_string('%s'%me['hist_%s_%s_%s' % (r,g,b)])))
      query.append("'0' ) ASC")
      query = string.join(query, '')
      cursor.execute(query)
      output = [ ]
      row = cursor.fetchone ()
      if row != None:
        id = row['id']
        output.append(focustable(id))
        output.append(thumbnailtable('colour', fetchids(cursor)))
      else:
        output = emptyOutput
    else:
      output = emptyOutput
    count = getcount(conn)
    coords = getcoords(conn, self.id)
    conn.close ()
    return theme_page('colour', string.join(output, ''), self.id, count, coords)

# ======================================================================
# gallery nearby class
class GalleryNearby(object):
  def __init__(self, req, id):
    self.request = req
    self.id = id
  def get(self):
    conn = dbconnect ()
    cursor = conn.cursor (MySQLdb.cursors.DictCursor)
    cursor.execute ("SELECT ru,rv,f,k FROM rdex WHERE id = '%s'" % conn.escape_string('%s'%self.id))
    me = cursor.fetchone()
    if me != None:
      cursor.close ()
      cursor = conn.cursor (MySQLdb.cursors.DictCursor)
      cursor.execute ('''
        SELECT id FROM rdex ORDER BY (
        ((ru - '%s') * (ru - '%s') * 4) + ((rv - '%s') * (rv - '%s') * 4) +
        ((f - '%s') * (f - '%s') * 10) + ((k - '%s') * (k - '%s') * 10) ) ASC
        ''' % (
        conn.escape_string('%s'%me['ru']), conn.escape_string('%s'%me['ru']),
        conn.escape_string('%s'%me['rv']), conn.escape_string('%s'%me['rv']),
        conn.escape_string('%s'%me['f']), conn.escape_string('%s'%me['f']),
        conn.escape_string('%s'%me['k']), conn.escape_string('%s'%me['k']) ) )
      output = [ ]
      row = cursor.fetchone ()
      if row != None:
        id = row['id']
        output.append(focustable(id))
        output.append(thumbnailtable('nearby', fetchids(cursor)))
      else:
        output = emptyOutput
    else:
      output = emptyOutput
    count = getcount(conn)
    coords = getcoords(conn, self.id)
    conn.close ()
    return theme_page('nearby', string.join(output, ''), self.id, count, coords)

# ======================================================================
# gallery segment class
class GallerySegment(object):
  def __init__(self, req, id):
    self.request = req
    self.id = id
  def get(self):
    conn = dbconnect ()
    cursor = conn.cursor (MySQLdb.cursors.DictCursor)
    query = []
    query.append("SELECT id")
    for s in range(0,segment_buckets):
      query.append(",seg_%s" % s)
    query.append(" FROM rdex WHERE id = '%s'" % conn.escape_string('%s'%self.id))
    query = string.join(query, '')
    cursor.execute(query)
    me = cursor.fetchone()
    if me != None:
      cursor.close ()
      cursor = conn.cursor (MySQLdb.cursors.DictCursor)
      query = []
      query.append("SELECT id FROM rdex ORDER BY (")
      for s in range(0,segment_buckets):
        query.append("ABS(seg_%s - '%s') + " % (s, conn.escape_string('%s'%me['seg_%s' % s])))
      query.append("'0' ) ASC")
      query = string.join(query, '')
      cursor.execute(query)
      output = [ ]
      row = cursor.fetchone ()
      if row != None:
        id = row['id']
        output.append(focustable(id))
        output.append(thumbnailtable('segment', fetchids(cursor)))
      else:
        output = emptyOutput
    else:
      output = emptyOutput
    count = getcount(conn)
    coords = getcoords(conn, self.id)
    conn.close ()
    return theme_page('segment', string.join(output, ''), self.id, count, coords)

# ======================================================================
# database rss feed class
class DownloadRSS(object):
  def __init__(self, req):
    self.request = req
  def get(self):
    conn = dbconnect ()
    cursor = conn.cursor (MySQLdb.cursors.DictCursor)
    cursor.execute ("SELECT id,ru,rv,f,k FROM rdex ORDER BY id DESC")
    output = [ ]
    output.append('''<?xml version="1.0"?>
<rss version="2.0">
<channel>
<title>rdex :: %s</title>
<link>http://%s%s</link>
<description>rdex chronology</description>
     ''' % ( mountpointname, host, mountpoint ) )
    results = cursor.fetchall ()
    if cursor.rowcount > 0:
      for row in results:
        output.append('''<item>
  <title>%s</title>
  <description>ru = %s ; rv = %s ; f = %s ; k = %s</description>
  <enclosure url="http://%s%s/images/%s.png" type="image/png" />
</item>''' % ( row['id'], row['ru'], row['rv'], row['f'], row['k'], host, mountpoint, row['id'] ) )
    output.append("</channel></rss>")
    cursor.close ()
    conn.close ()
    return string.join(output, '')


# ======================================================================
# database txt feed class
class DownloadTXT(object):
  def __init__(self, req):
    self.request = req
  def get(self):
    conn = dbconnect ()
    cursor = conn.cursor (MySQLdb.cursors.DictCursor)
    cursor.execute ("SELECT id,ru,rv,f,k FROM rdex ORDER BY id ASC")
    output = [ ]
    output.append("id,ru,rv,f,k")
    results = cursor.fetchall ()
    if cursor.rowcount > 0:
      for row in results:
        output.append("%s,%s,%s,%s,%s" % ( row['id'], row['ru'], row['rv'], row['f'], row['k'] ) )
    cursor.close ()
    conn.close ()
    return string.join(output, "\n")


# ======================================================================
# database new item class
class DbNew(object):
  def __init__(self, req):
    self.request = req
  def get(self):
    return theme_page('new', '<p>use rdex-client to submit to the database</p>', None, None)
  def post(self):
    try:
      conn = dbconnect ()
      cursor = conn.cursor ()
      image = Image.open(self.request.params['image'].file)
      query = [ ]
      query.append("INSERT INTO rdex ( ")
      query.append("ru,rv,f,k,behaviour,coarseness_u,contrast_u,directionality_u,coarseness_v,contrast_v,directionality_v")
      for r in range(0,colour_buckets):
        for g in range(0,colour_buckets):
          for b in range(0,colour_buckets):
            query.append(",hist_%s_%s_%s" % (r,g,b))
      for s in range(0,segment_buckets):
        query.append(",seg_%s" % s)
      query.append(") VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'")
      for r in range(0,colour_buckets):
        for g in range(0,colour_buckets):
          for b in range(0,colour_buckets):
            query.append(",'%s'")
      for s in range(0,segment_buckets):
        query.append(",'%s'")
      query.append(")")
      query = string.join(query, '')
      params = []
      params.append(conn.escape_string('%s'%self.request.params['ru']))
      params.append(conn.escape_string('%s'%self.request.params['rv']))
      params.append(conn.escape_string('%s'%self.request.params['f']))
      params.append(conn.escape_string('%s'%self.request.params['k']))
      params.append(conn.escape_string('%s'%self.request.params['behaviour']))
      params.append(conn.escape_string('%s'%self.request.params['coarseness_u']))
      params.append(conn.escape_string('%s'%self.request.params['contrast_u']))
      params.append(conn.escape_string('%s'%self.request.params['directionality_u']))
      params.append(conn.escape_string('%s'%self.request.params['coarseness_v']))
      params.append(conn.escape_string('%s'%self.request.params['contrast_v']))
      params.append(conn.escape_string('%s'%self.request.params['directionality_v']))
      for r in range(0,colour_buckets):
        for g in range(0,colour_buckets):
          for b in range(0,colour_buckets):
            params.append(conn.escape_string('%s'%self.request.params['hist_%s_%s_%s' % (r, g, b)]))
      for s in range(0,segment_buckets):
        params.append(conn.escape_string('%s'%self.request.params['seg_%s' % s]))
      cursor.execute(query % tuple(params))
      inserted = conn.insert_id ()
      image.save('%s/%s.png' % (image_dir, inserted))
      image2 = image.crop((0,0,128,128))
      image2.thumbnail(thumbnailsize, Image.ANTIALIAS)
      image2.save('%s/%s.jpg' % (image_dir, inserted), quality=90)
      result = '%s' % inserted
    except:
      result = exc.HTTPInternalServerError('bad POST')
    finally:
      cursor.close ()
      conn.commit ()
      conn.close ()
    return result

# ======================================================================
# build application routes
application = Router()
application.add_route('',                  Redirect('%s/' % ( mountpoint )))
application.add_route('/',                 Redirect('%s/nearby' % ( mountpoint )))
application.add_route('/nearby',           Redirect('%s/nearby/' % ( mountpoint )))
application.add_route('/colour',           Redirect('%s/colour/' % ( mountpoint )))
application.add_route('/texture',          Redirect('%s/texture/' % ( mountpoint )))
application.add_route('/segment',          Redirect('%s/segment/' % ( mountpoint )))
application.add_route('/nearby/',          Redirect('%s/nearby/latest' % ( mountpoint )))
application.add_route('/colour/',          Redirect('%s/colour/latest' % ( mountpoint )))
application.add_route('/texture/',         Redirect('%s/texture/latest' % ( mountpoint )))
application.add_route('/segment/',         Redirect('%s/segment/latest' % ( mountpoint )))
application.add_route('/nearby/latest',    RedirectLatest('%s/nearby' % ( mountpoint )))
application.add_route('/colour/latest',    RedirectLatest('%s/colour' % ( mountpoint )))
application.add_route('/texture/latest',   RedirectLatest('%s/texture' % ( mountpoint )))
application.add_route('/segment/latest',   RedirectLatest('%s/segment' % ( mountpoint )))
application.add_route('/nearby/random',    RedirectRandom('%s/nearby' % ( mountpoint )))
application.add_route('/colour/random',    RedirectRandom('%s/colour' % ( mountpoint )))
application.add_route('/texture/random',   RedirectRandom('%s/texture' % ( mountpoint )))
application.add_route('/segment/random',   RedirectRandom('%s/segment' % ( mountpoint )))
application.add_route('/nearby/{id:\d+}',  rest_controller(GalleryNearby))
application.add_route('/colour/{id:\d+}',  rest_controller(GalleryColour))
application.add_route('/texture/{id:\d+}', rest_controller(GalleryTexture))
application.add_route('/segment/{id:\d+}', rest_controller(GallerySegment))
application.add_route('/download/rss',     rest_controller(DownloadRSS, "text/xml"))
application.add_route('/download/txt',     rest_controller(DownloadTXT, "text/plain"))
application.add_route('/upload/new',       rest_controller(DbNew))

# ======================================================================
# limit upload size class
# http://wiki.pylonshq.com/display/pylonscookbook/A+Better+Way+To+Limit+File+Upload+Size
class LimitUploadSize(object):
  def __init__(self, app, size):
    self.app = app
    self.size = size
  def __call__(self, environ, start_response):
    req = Request(environ)
    if req.method=='POST':
      len = req.headers.get('Content-length')
      if not len:
        return exc.HTTPBadRequest("No content-length header specified")(environ, start_response)
      elif int(len) > self.size:
        return exc.HTTPBadRequest("POST body exceeds maximum limits")(environ, start_response)
    resp = req.get_response(self.app)
    return resp(environ, start_response)

# ======================================================================
# main application
application = LimitUploadSize(application, 666666)

# EOF
